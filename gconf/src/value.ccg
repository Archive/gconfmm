// -*- Mode: C++; c-basic-offset: 4  -*-
/* $Id: value.ccg,v 1.2 2005/01/11 12:08:16 murrayc Exp $ */

/* value.hg
 * 
 * Copyright (C) 2000-2002 GConfmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gconf/gconf.h>
#include "gconfmm/schema.h"

namespace
{

//gmmproc guesses the gconf prefix wrongly, so we help it:
inline static GType g_conf_value_type_get_type()
{
  return gconf_value_type_get_type();
}

inline static GType g_conf_unset_flags_get_type()
{
  return gconf_unset_flags_get_type();
}

} //anonymous namespace

namespace Gnome
{

namespace Conf
{

void ValueTraits::release_c_type(GConfValue* ptr)
{
  gconf_value_free(ptr);
}
  

GConfValue* ValueSchemaTraits::to_c_type(const Schema& ptr)
{
  GConfValue* v = gconf_value_new(GCONF_VALUE_SCHEMA);
  gconf_value_set_schema(v, ptr.gobj());
  return v;
}

Schema ValueSchemaTraits::to_cpp_type(GConfValue* ptr)
{
  return Schema(gconf_value_get_schema(ptr), true);
}

GConfValue* ValueStringTraits::to_c_type(const Glib::ustring& ptr)
{
  GConfValue* v = gconf_value_new(GCONF_VALUE_STRING);
  gconf_value_set_string(v,ptr.c_str());
  return v;
}

Glib::ustring ValueStringTraits::to_cpp_type(GConfValue* ptr)
{
  return Glib::ustring(gconf_value_get_string(ptr));
}

GConfValue* ValueIntTraits::to_c_type(const int& ptr)
{
  GConfValue* v = gconf_value_new(GCONF_VALUE_INT);
  gconf_value_set_int(v,ptr);
  return v;
}

int ValueIntTraits::to_cpp_type(GConfValue* ptr)
{
  return gconf_value_get_int(ptr);
}

GConfValue* ValueBoolTraits::to_c_type(const bool& ptr)
{
  GConfValue* v = gconf_value_new(GCONF_VALUE_BOOL);
  gconf_value_set_bool(v, ptr);
  return v;
}

bool ValueBoolTraits::to_cpp_type(GConfValue* ptr)
{
  return (bool)gconf_value_get_bool(ptr);
}

GConfValue* ValueFloatTraits::to_c_type(const double& ptr)
{
  GConfValue* v = gconf_value_new(GCONF_VALUE_FLOAT);
  gconf_value_set_float(v,ptr);
  return v;
}

double ValueFloatTraits::to_cpp_type(GConfValue* ptr)
{
  return gconf_value_get_float(ptr);
}


Value::Value(ValueType type)
  : gobject_(0)
{
  if(type != VALUE_INVALID)
    gobject_ = gconf_value_new((GConfValueType)type);
}

ValueType Value::get_type() const 
{
  if(gobject_ == 0)
    return VALUE_INVALID;

  return (ValueType)gobj()->type;
}

Value Value::get_car() const
{
  return Value(gconf_value_get_car(gobj()), true);
}

Value Value::get_cdr() const
{
  return Value(gconf_value_get_cdr(gobj()), true);
}

Schema Value::get_schema() const
{
  return Schema(gconf_value_get_schema(gobj()), true);
}

} /* namespace Conf */
} /* namespace Gnome */
