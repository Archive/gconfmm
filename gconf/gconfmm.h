/* $Id$ */
/* gconfmm - a C++ wrapper for GConf
 *
 * Copyright 1999-2001 Free Software Foundation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GCONFMM_H
#define GCONFMM_H

/** @mainpage gconfmm Reference Manual
 *
 * @section description Description
 *
 * gconfmm is the official C++ interface for the %GConf client API for 
 * storing and retrieving configuration data. See Gnome::Conf::Client.
 *
 * @section basics Basic Usage
 *
 * Include the gconfmm header:
 * @code
 * #include <gconfmm.h>
 * @endcode
 * (You may include individual headers, such as @c gconfmm/client.h instead.)
 *
 * If your source file is @c program.cc, you can compile it with:
 * @code
 * g++ program.cc -o program  `pkg-config --cflags --libs gconfmm-2.6`
 * @endcode
 *
 * Alternatively, if using autoconf, use the following in @c configure.ac:
 * @code
 * PKG_CHECK_MODULES([GCONFMM], [gconfmm-2.4])
 * @endcode
 * Then use the generated @c GCONFMM_CFLAGS and @c GCONFMM_LIBS variables in the
 * project Makefile.am files. For example:
 * @code
 * program_CPPFLAGS = $(GCONFMM_CFLAGS)
 * program_LDADD = $(GCONFMM_LIBS)
 * @endcode
 */
 
/* gconfmm version.  */
extern const int gconfmm_major_version;
extern const int gconfmm_minor_version;
extern const int gconfmm_micro_version;

#include <gconfmm/init.h>
#include <gconfmm/client.h>
#include <gconfmm/value.h>
#include <gconfmm/schema.h>
#include <gconfmm/entry.h>


#endif /* #ifndef GCONFMM_H */
